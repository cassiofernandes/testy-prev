import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'carta',
    loadChildren: () => import('./carta/carta.module').then(m => m.CartaModule)
  },
  { 
    path: '',   
    redirectTo: '/carta', 
    pathMatch: 'full' 
  } 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
