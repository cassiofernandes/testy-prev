import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { PokemonService } from 'src/app/server/pokemon.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  estado: boolean = true;

  data: any;

  constructor(
    private pokemonService: PokemonService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.details()
  }

  details() {
    this.route.params
      .pipe(
        map((params: any) => params['id']),
        switchMap((id: string) => this.pokemonService.getDetails(id))
      )
      .subscribe((res: any) => {
        this.data = res['data']
        this.estado = false;
      })
  }
}
