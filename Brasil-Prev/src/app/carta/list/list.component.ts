import { Component, OnInit, ViewChild } from '@angular/core';
import { PokemonService } from 'src/app/server/pokemon.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  estado: boolean = true;

  displayedColumns: string[] = ['id', 'name', 'types', 'image'];
  
  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;

  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private server: PokemonService,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  dataSource: any; 

  searchForm = this.fb.group({
    name: [''],
  });

  ngOnInit(): void {    
    this.listPokemon();
  }

  listPokemon() {
    this.server.getList()
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(
        (res: any) => {
    
          this.dataSource = new MatTableDataSource<any[]>(res);
          this.dataSource.paginator = this.paginator;

          this.dataSource.filterPredicate = (data: any, filter: string) => {
            return data.name.toLocaleLowerCase().includes(filter);
          }; 
          this.estado = false;      
        }
      )
  }

  applyFilter() {
    this.dataSource.filter = this.searchForm.value.name.trim().toLowerCase();  
  }

  details(dados: any) {
    this.router.navigate(['detalhe', dados], { relativeTo: this.route})
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}
