import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map, retry, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor(
    private http: HttpClient
  ) { }

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  order(objs: any) {
    let obj = objs['data']
    return obj.sort((a: any, b: any) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
  }

  getList(): Observable<any[]> {
    return this.http.get<any[]>(`${environment.API}`, this.httpOptions)
      .pipe(
        map(res => this.order(res)),
        catchError((error: any) => this.handleError(error))        
      )
    }
  
  getDetails(id: string): Observable<any[]> {
    return this.http.get<any[]>(`${environment.API}/${id}`, this.httpOptions)
      .pipe(
        retry(1),
        catchError((error: any) => this.handleError(error)) 
      )
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

}
